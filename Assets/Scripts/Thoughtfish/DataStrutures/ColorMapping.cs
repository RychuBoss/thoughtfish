﻿using System;
using UnityEngine;

namespace Thoughtfish.DataStrutures
{
    [Serializable]
    public class ColorMapping
    {
        public ShapeColor shapeColor;
        public Color color;
    }
}
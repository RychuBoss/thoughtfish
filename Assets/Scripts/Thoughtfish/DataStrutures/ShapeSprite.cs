﻿using System;
using UnityEngine;

namespace Thoughtfish.DataStrutures
{
    [Serializable]
    public class ShapeSprite
    {
        public Shape shape;
        public Sprite sprite;
    }
}
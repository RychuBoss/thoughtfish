﻿namespace Thoughtfish.DataStrutures
{
    public enum ShapeColor
    {
        Yellow,
        Blue,
        Red,
        Green,
    }
}
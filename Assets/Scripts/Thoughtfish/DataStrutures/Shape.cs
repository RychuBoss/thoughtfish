﻿namespace Thoughtfish.DataStrutures
{
    public enum Shape
    {
        Circle,
        Elipse,
        Square,
        Rectangle
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using Thoughtfish.Configration;
using Thoughtfish.DataStrutures;
using Thoughtfish.Views;
using UnityEngine;
using UnityEngine.Assertions;
using Random = UnityEngine.Random;

namespace Thoughtfish
{
    public class ButtonManager : MonoBehaviour
    {
        [SerializeField]
        private ButtonConfiguration configuration;

        [SerializeField]
        private MyButton buttonPrefab;

        private readonly List<MyButton> buttons = new List<MyButton>();

        private void Awake()
        {
            Assert.IsNotNull(configuration);
            Assert.IsNotNull(buttonPrefab);
        }

        private void Start()
        {
            SpawnButtons();
        }

        private void SpawnButtons()
        {
            for (int i = 0; i < configuration.buttonsToSpawn; ++i)
            {
                SpawnButton();
            }
        }

        private void SpawnButton()
        {
            var position = GeneratePosition();

            var button = Instantiate(buttonPrefab, position, Quaternion.identity);
            buttons.Add(button);
            
            button.transform.localScale = GenerateScale();
            button.SetShape(PickShape());
            button.SetColor(PickColor());
            button.GenerateId();
            button.Init(configuration);
        }

        private ColorMapping PickColor()
        {
            int colorIndex = Random.Range(0, configuration.colors.Length);
            return configuration.colors[colorIndex];
        }

        private ShapeSprite PickShape()
        {
            int shapeIndex = Random.Range(0, configuration.shapes.Length);
            return configuration.shapes[shapeIndex];
        }

        private Vector3 GenerateScale()
        {
            var scaleFactor = Random.Range(configuration.minScale, configuration.maxScale);
            return Vector3.one * scaleFactor;
        }

        private Vector3 GeneratePosition()
        {
            var x = Random.Range(configuration.minX, configuration.maxX);
            var y = Random.Range(configuration.minY, configuration.maxY);

            return new Vector3(x, y, 0);
        }

        public MyButton GetButton(string id)
        {
            return buttons.First(x => x.GetId() == id);
        }
    }
}

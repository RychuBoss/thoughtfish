﻿using System;
using System.Linq;
using Thoughtfish.Configration;
using Thoughtfish.DataStrutures;
using Thoughtfish.Events;
using UnityEngine;
using UnityEngine.Assertions;

namespace Thoughtfish.Views
{
    public class MyButton : MonoBehaviour
    {
        [SerializeField]
        private SpriteRenderer spriteRenderer;

        [SerializeField]
        private SpriteRenderer highlight;

        [SerializeField]
        private SpriteRenderer dragSprite;

        private Component shapeCollider;
        private string id;
        private Shape currentShape;
        private ShapeColor currentColor;

        private ButtonConfiguration configuration;

        private void Awake()
        {
            Assert.IsNotNull(spriteRenderer);
            Assert.IsNotNull(highlight);
            Assert.IsNotNull(dragSprite);
        }

        public void Init(ButtonConfiguration config)
        {
            configuration = config;
        }

        public void SetShape(ShapeSprite shape)
        {
            currentShape = shape.shape;
            
            spriteRenderer.sprite = shape.sprite;
            highlight.sprite = shape.sprite;
            dragSprite.sprite = shape.sprite;

            SetAppropriateCollider(shape);
        }

        private void SetAppropriateCollider(ShapeSprite shape)
        {
            if (shapeCollider)
            {
                Destroy(shapeCollider);
            }

            switch (shape.shape)
            {
                case Shape.Rectangle:
                case Shape.Square:
                    shapeCollider = gameObject.AddComponent<BoxCollider2D>();
                    break;
                case Shape.Circle:
                    shapeCollider = gameObject.AddComponent<CircleCollider2D>();
                    break;
                case Shape.Elipse:
                    shapeCollider = gameObject.AddComponent<CapsuleCollider2D>();
                    break;
            }
        }

        public void SetColor(ColorMapping shapeColor)
        {
            currentColor = shapeColor.shapeColor;
            var color = shapeColor.color;
            spriteRenderer.color = color;
            color.a = 0.5f;
            dragSprite.color = color;
        }

        public void ShowHighlight()
        {
            highlight.gameObject.SetActive(true);
        }

        public void HideHighlight()
        {
            highlight.gameObject.SetActive(false);
        }

        public void SetDragSpritePosition(Vector3 mappedMousePosition)
        {
            dragSprite.transform.position = mappedMousePosition;
            dragSprite.gameObject.SetActive(true);
        }

        public void MoveToPosition(Vector3 newPosition)
        {
            transform.position = newPosition;
            dragSprite.gameObject.SetActive(false);
        }

        public void GenerateId()
        {
            id = Guid.NewGuid().ToString();
        }

        public string GetId()
        {
            return id;
        }

        public void SwitchToNextShape()
        {
            var allShapes = Enum.GetNames(typeof(Shape)).Length;
            var nextShapeIndex = ((int) currentShape  + 1) % allShapes;
            var nextShape = (Shape) nextShapeIndex;
            var spriteShape = configuration.shapes.First(x => x.shape == nextShape);

            SendShapeChangedEvent(nextShape);

            SetShape(spriteShape);
        }

        private void SendShapeChangedEvent(Shape nextShape)
        {
            var shapedChangedEventData = new ShapedChangedEventData
            {
                time = Time.time,
                shapeId = id,
                initialShape = currentShape,
                targetShape = nextShape
            };
            ShapeChangedEvent.Invoke(shapedChangedEventData);
        }

        public void SwitchToNextColor()
        {
            var allColors = Enum.GetNames(typeof(ShapeColor)).Length;
            var nextColorIndex = ((int) currentColor  + 1) % allColors;
            var nextcolor = (ShapeColor) nextColorIndex;
            var spriteShape = configuration.colors.First(x => x.shapeColor == nextcolor);
            SetColor(spriteShape);
        }

        public void SetShape(Shape shape)
        {
            var shapeSprite = configuration.shapes.First(x => x.shape == shape);
            SetShape(shapeSprite);
        }
    }
}
﻿using Thoughtfish.Recording;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace Thoughtfish.TopMenu
{
    public class StopRecordingButton : MonoBehaviour
    {
        private Button button;
        private IActionRecorder actionRecorder;

        private void Awake()
        {
            button = GetComponent<Button>();
            Assert.IsNotNull(button);
        }

        public void Init(IActionRecorder actionRecorder)
        {
            this.actionRecorder = actionRecorder;
        }

        private void Start()
        {
            button.onClick.AddListener(StopRecording);
        }

        private void StopRecording()
        {
            actionRecorder.StopRecording();
        }

        private void OnDestroy()
        {
            button.onClick.RemoveListener(StopRecording);
        }
    }
}
﻿using Thoughtfish.Popups;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace Thoughtfish.TopMenu
{
    public class PlayRecordedActionsButton : MonoBehaviour
    {
        private Button button;
        private IPopupsManager popupsManager;

        private void Awake()
        {
            button = GetComponent<Button>();
            Assert.IsNotNull(button);
        }

        public void Init(IPopupsManager popupsManager)
        {
            this.popupsManager = popupsManager;
        }

        private void Start()
        {
            button.onClick.AddListener(ShowAvailableReplays);
        }

        private void ShowAvailableReplays()
        {
            popupsManager.ShowAvailableRecordingsPopup();
        }

        private void OnDestroy()
        {
            button.onClick.RemoveListener(ShowAvailableReplays);
        }
    }
}
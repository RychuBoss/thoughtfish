﻿using TMPro;
using UnityEngine;
using UnityEngine.Assertions;

namespace Thoughtfish.TopMenu
{
    public class TopMenuCommandView : MonoBehaviour
    {
        [SerializeField]
        private TextMeshProUGUI label;

        private void Awake()
        {
            Assert.IsNotNull(label);
        }

        public void SetName(string name)
        {
            label.text = name;
        }
    }
}
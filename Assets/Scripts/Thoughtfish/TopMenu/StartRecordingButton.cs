﻿using Thoughtfish.Popups;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace Thoughtfish.TopMenu
{
    public class StartRecordingButton : MonoBehaviour
    {
        private Button button;

        private IPopupsManager popupsManager;

        private void Awake()
        {
            button = GetComponent<Button>();
            Assert.IsNotNull(button);
        }

        public void Init(IPopupsManager popupsManager)
        {
            this.popupsManager = popupsManager;
        }

        private void Start()
        {
            button.onClick.AddListener(StartRecording);
        }

        private void StartRecording()
        {
            popupsManager.ShowStartRecordingPopup();
        }

        private void OnDestroy()
        {
            button.onClick.RemoveListener(StartRecording);
        }
    }
}
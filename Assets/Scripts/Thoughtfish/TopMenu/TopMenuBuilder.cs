﻿using System.Collections.Generic;
using System.Linq;
using Thoughtfish.Configration;
using Thoughtfish.Popups;
using Thoughtfish.Recording;
using UnityEngine;
using UnityEngine.Assertions;

namespace Thoughtfish.TopMenu
{
    public class TopMenuBuilder : MonoBehaviour
    {
        [SerializeField]
        private PopupsManager popupsManager;

        [SerializeField]
        private ActionRecorder actionRecorder;

        [SerializeField]
        private MenuStructureConfig menuStructure;

        [SerializeField]
        private TopMenuCommandView commandPrefab;

        private readonly List<TopMenuCommandView> commands = new List<TopMenuCommandView>();

        private void Awake()
        {
            Assert.IsNotNull(menuStructure);
            Assert.IsNotNull(popupsManager);
            Assert.IsNotNull(actionRecorder);
            Assert.IsNotNull(commandPrefab);
        }

        private void Start()
        {
            BuildMenu();
        }

        private void BuildMenu()
        {
            foreach (var command in menuStructure.order)
            {
                BuildCommandButton(command);
            }
        }

        private void BuildCommandButton(TopMenuCommand command)
        {
            var commandConfig = menuStructure.itemsConfiguration.First(x => x.command == command);
            var commandView = Instantiate(commandPrefab, transform);
            ConfigureCommandView(commandView, commandConfig);
            commands.Add(commandView);
        }

        private void ConfigureCommandView(TopMenuCommandView commandView, TopMenuItem commandConfig)
        {
            commandView.SetName(commandConfig.name);
            switch (commandConfig.command)
            {
                case TopMenuCommand.StartRecording:
                    var startRecordingButton = commandView.gameObject.AddComponent<StartRecordingButton>();
                    startRecordingButton.Init(popupsManager);
                    break;
                case TopMenuCommand.StopRecording:
                    var button = commandView.gameObject.AddComponent<StopRecordingButton>();
                    button.Init(actionRecorder);
                    break;
                case TopMenuCommand.LoadRecordedEvents:
                    var replayButton = commandView.gameObject.AddComponent<PlayRecordedActionsButton>();
                    replayButton.Init(popupsManager);
                    break;
            }
        }
    }
}
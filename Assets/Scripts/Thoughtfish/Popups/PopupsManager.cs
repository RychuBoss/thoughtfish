﻿using Thoughtfish.Recording;
using UnityEngine;
using UnityEngine.Assertions;

namespace Thoughtfish.Popups
{
    public class PopupsManager : MonoBehaviour, IPopupsManager
    {
        [SerializeField]
        private ActionRecorder actionRecorder;
        
        [SerializeField]
        private StartRecordingPopup startRecordingPopup;

        [SerializeField]
        private AvailableRecordingsPopup availableRecordingsPopup;

        private void Awake()
        {
            Assert.IsNotNull(actionRecorder);
            Assert.IsNotNull(startRecordingPopup);
        }

        private void Start()
        {
            startRecordingPopup.Init(actionRecorder);
            availableRecordingsPopup.Init(actionRecorder);
        }

        public void ShowStartRecordingPopup()
        {
            startRecordingPopup.gameObject.SetActive(true);
        }

        public void ShowAvailableRecordingsPopup()
        {
            availableRecordingsPopup.gameObject.SetActive(true);
        }
    }
}
﻿using Thoughtfish.Recording;
using TMPro;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace Thoughtfish.Popups
{
    public class StartRecordingPopup : MonoBehaviour
    {
        private IActionRecorder actionRecorder;

        [SerializeField]
        private TMP_InputField recordingNameField;

        [SerializeField]
        private Button startRecordingButton;

        [SerializeField]
        private Button closeButton;

        private void Awake()
        {
            Assert.IsNotNull(recordingNameField);
            Assert.IsNotNull(startRecordingButton);
            Assert.IsNotNull(closeButton);
        }

        private void Start()
        {
            startRecordingButton.onClick.AddListener(StartRecording);
            closeButton.onClick.AddListener(Close);
            recordingNameField.onSubmit.AddListener(StartRecording);
        }

        public void Init(IActionRecorder recorder)
        {
            actionRecorder = recorder;
        }

        private void StartRecording(string name)
        {
            actionRecorder.StartRecording(name);
            Close();
        }

        private void StartRecording()
        {
            StartRecording(recordingNameField.text);
        }

        private void Close()
        {
            gameObject.SetActive(false);
        }

        private void OnDestroy()
        {
            startRecordingButton.onClick.RemoveListener(StartRecording);
            recordingNameField.onSubmit.RemoveListener(StartRecording);
            closeButton.onClick.RemoveListener(Close);
        }
    }
}
﻿using Thoughtfish.Recording;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.UI;

namespace Thoughtfish.Popups
{
    public class AvailableRecordingsPopup : MonoBehaviour
    {
        [SerializeField]
        private Button replayRecordingButton;

        [SerializeField]
        private Button closeButton;

        private IActionRecorder actionRecorder;

        private void Awake()
        {
            Assert.IsNotNull(replayRecordingButton);
            Assert.IsNotNull(closeButton);
        }

        private void Start()
        {
            replayRecordingButton.onClick.AddListener(ReplayRecording);
            closeButton.onClick.AddListener(Close);
        }

        public void Init(IActionRecorder recorder)
        {
            actionRecorder = recorder;
        }

        private void ReplayRecording()
        {
            Close();
            actionRecorder.Replay();
        }

        private void Close()
        {
            gameObject.SetActive(false);
        }

        private void OnDestroy()
        {
            replayRecordingButton.onClick.RemoveListener(ReplayRecording);
            closeButton.onClick.RemoveListener(Close);
        }
    }
}
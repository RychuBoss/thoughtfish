﻿namespace Thoughtfish.Popups
{
    public interface IPopupsManager
    {
        void ShowStartRecordingPopup();
        void ShowAvailableRecordingsPopup();
    }
}
﻿using UnityEngine;

namespace Thoughtfish.Events
{
    public static class ShapeMovedEvent
    {
        public delegate void ShapeMovedDelegate(ShapedMovedEventData data);

        public static event ShapeMovedDelegate shapeMovedEvent;

        public static void Invoke(ShapedMovedEventData data)
        {
            shapeMovedEvent?.Invoke(data);
        }
    }

    public class ShapedMovedEventData
    {
        public float time;
        public string shapeId;
        public Vector3 initialPosition;
        public Vector3 targetPosition;
    }
}
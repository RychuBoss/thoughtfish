﻿using Thoughtfish.DataStrutures;

namespace Thoughtfish.Events
{
    public class ShapeChangedEvent
    {
        public delegate void ShapeChangedDelegate(ShapedChangedEventData data);

        public static event ShapeChangedDelegate shapeChangedEvent;

        public static void Invoke(ShapedChangedEventData data)
        {
            shapeChangedEvent?.Invoke(data);
        }
    }

    public class ShapedChangedEventData
    {
        public float time;
        public string shapeId;
        public Shape initialShape;
        public Shape targetShape;
    }
}
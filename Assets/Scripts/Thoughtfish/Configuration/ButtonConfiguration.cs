﻿using Thoughtfish.DataStrutures;
using UnityEngine;

namespace Thoughtfish.Configration
{
    [CreateAssetMenu(menuName = "Thoughtfish/ButtonConfiguration", fileName = "ButtonConfiguration")]
    public class ButtonConfiguration : ScriptableObject
    {
        public int buttonsToSpawn;
        
        public float minScale = .3f;
        public float maxScale = 10f;

        public float minX;
        public float maxX;
        public float minY;
        public float maxY;

        public ShapeSprite[] shapes;
        public ColorMapping[] colors;
    }
}
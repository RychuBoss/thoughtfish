﻿using System;
using UnityEngine;

namespace Thoughtfish.Configration
{
    [CreateAssetMenu(menuName = "Thoughtfish/MenuStructureConfig", fileName = "MenuStructureConfig")]
    public class MenuStructureConfig : ScriptableObject
    {
        public TopMenuCommand[] order;

        public TopMenuItem[] itemsConfiguration;
    }

    [Serializable]
    public class TopMenuItem
    {
        public TopMenuCommand command;
        public string name;
    }

    public enum TopMenuCommand
    {
        StartRecording,
        StopRecording,
        LoadRecordedEvents
    }
}
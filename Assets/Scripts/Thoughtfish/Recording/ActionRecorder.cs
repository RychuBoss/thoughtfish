﻿using System;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using DG.Tweening;
using Newtonsoft.Json;
using Thoughtfish.Events;
using UnityEngine;
using UnityEngine.Assertions;

namespace Thoughtfish.Recording
{
    [Serializable]
    public class ActionRecorder : MonoBehaviour, IActionRecorder
    {
        [SerializeField]
        private ButtonManager buttonManager;

        private const string RecordingsKey = "recordings_key";
        private const float ShapeMoveDuration = 0.5f;
        private ActionsRecord currentRecord;
        private float recordingStartTime;

        private void Awake()
        {
            Assert.IsNotNull(buttonManager);

            //Needed for vector3 serialization for some reason
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };
        }

        public void StartRecording(string name)
        {
            recordingStartTime = Time.time;
            currentRecord = new ActionsRecord(name);
            ShapeMovedEvent.shapeMovedEvent += OnShapeMoved;
            ShapeChangedEvent.shapeChangedEvent += OnShapeChanged;
        }

        public void StopRecording()
        {
            Debug.Log("Stopped recording");
            ShapeMovedEvent.shapeMovedEvent -= OnShapeMoved;
            ShapeChangedEvent.shapeChangedEvent -= OnShapeChanged;

            SaveRecord();
        }

        public void Replay()
        {
            var record = LoadRecord();
            if (record != null)
            {
                PlayAllCommands(record);
            }
        }

        private void OnShapeMoved(ShapedMovedEventData data)
        {
            data.time -= recordingStartTime;

            currentRecord.commandsList.Add(new CommandIdentity(ShapeCommand.Move, data.shapeId, data.time));
            currentRecord.movedShapesData.Add(data);
        }

        private void OnShapeChanged(ShapedChangedEventData data)
        {
            data.time -= recordingStartTime;

            currentRecord.commandsList.Add(new CommandIdentity(ShapeCommand.Change, data.shapeId, data.time));
            currentRecord.changedShapesData.Add(data);
        }

        private void PlayAllCommands(ActionsRecord record)
        {
            ResetCommandTargets(record);
            foreach (var identity in record.commandsList)
            {
                ReplayCommand(record, identity);
            }
        }

        private void ResetCommandTargets(ActionsRecord record)
        {
            ResetMoveCommandTargets(record);
            ResetChangeShapeTargets(record);
        }

        private void ResetChangeShapeTargets(ActionsRecord record)
        {
            var changeShapeTargets = new HashSet<string>();
            foreach (var dataEntry in record.changedShapesData)
            {
                changeShapeTargets.Add(dataEntry.shapeId);
            }

            foreach (var target in changeShapeTargets)
            {
                var data = record.changedShapesData.First(x => x.shapeId == target);
                var button = buttonManager.GetButton(target);
                button.SetShape(data.initialShape);
            }
        }

        private void ResetMoveCommandTargets(ActionsRecord record)
        {
            var moveTargets = new HashSet<string>();
            foreach (var dataEntry in record.movedShapesData)
            {
                moveTargets.Add(dataEntry.shapeId);
            }

            foreach (var target in moveTargets)
            {
                var data = record.movedShapesData.First(x => x.shapeId == target);
                var button = buttonManager.GetButton(target);
                button.transform.position = data.initialPosition;
            }
        }

        private void ReplayCommand(ActionsRecord record, CommandIdentity identity)
        {
            switch (identity.command)
            {
                case ShapeCommand.Move:

                    var commandData = record.movedShapesData.First(x =>
                        Math.Abs(x.time - identity.pointInTime) < 0.05f && x.shapeId == identity.targetId);
                    ExecuteShapeMovedCommand(commandData).Forget();
                    break;
                case ShapeCommand.Change:
                    var changeShapeData = record.changedShapesData.First(x =>
                        Math.Abs(x.time - identity.pointInTime) < 0.05f && x.shapeId == identity.targetId);
                    ExecuteChangeShapeCommand(changeShapeData).Forget();
                    break;
            }
        }

        private async UniTask ExecuteChangeShapeCommand(ShapedChangedEventData commandData)
        {
            await UniTask.Delay(TimeSpan.FromSeconds(commandData.time));
            var button = buttonManager.GetButton(commandData.shapeId);
            button.SetShape(commandData.targetShape);
        }

        private async UniTask ExecuteShapeMovedCommand(ShapedMovedEventData commandData)
        {
            await UniTask.Delay(TimeSpan.FromSeconds(commandData.time));
            var button = buttonManager.GetButton(commandData.shapeId);
            button.transform.DOMove(commandData.targetPosition, ShapeMoveDuration);
        }

        private static ActionsRecord LoadRecord()
        {
            var json = PlayerPrefs.GetString(RecordingsKey);
            var record = JsonConvert.DeserializeObject<ActionsRecord>(json);
            return record;
        }

        private void SaveRecord()
        {
            var json = JsonConvert.SerializeObject(currentRecord);
            PlayerPrefs.SetString(RecordingsKey, json);
            Debug.Log($"Saved record: {json}");
        }
    }

    public enum ShapeCommand
    {
        Highlight,
        Activate,
        Move,
        Change,
    }

    [Serializable]
    public class CommandIdentity
    {
        public ShapeCommand command;
        public string targetId;
        public float pointInTime;

        public CommandIdentity(ShapeCommand command, string targetId, float pointInTime)
        {
            this.command = command;
            this.targetId = targetId;
            this.pointInTime = pointInTime;
        }
    }

    [Serializable]
    public class ActionsRecord
    {
        public ActionsRecord(string name)
        {
            this.name = name;
        }

        public readonly string name;
        public readonly List<CommandIdentity> commandsList = new List<CommandIdentity>();
        public readonly List<ShapedMovedEventData> movedShapesData = new List<ShapedMovedEventData>();
        public readonly List<ShapedChangedEventData> changedShapesData = new List<ShapedChangedEventData>();
    }
}
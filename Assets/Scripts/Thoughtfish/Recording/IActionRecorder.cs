﻿namespace Thoughtfish.Recording
{
    public interface IActionRecorder
    {
        void StartRecording(string name);
        void StopRecording();
        void Replay();
    }
}
﻿using Thoughtfish.Views;
using UnityEngine;
using UnityEngine.Assertions;

namespace Thoughtfish.GestureDetection
{
    public class MouseOverDetector : MonoBehaviour
    {
        private const float TimeForHighlight = 0.5f;
        
        [SerializeField]
        private MyButton myButton;

        private float mouseOverStart;

        private Vector3 previousMousePosition = Vector3.one * 99999; 
        
        private void Awake()
        {
            Assert.IsNotNull(myButton);
        }

        private void OnEnable()
        {
            mouseOverStart = Time.time;
        }

        private void OnMouseOver()
        {
            var mousePosition = Input.mousePosition;
            if (mousePosition != previousMousePosition)
            {
                previousMousePosition = mousePosition;
                mouseOverStart = Time.time;
                myButton.HideHighlight();
            }
            else
            {
                if (Time.time - mouseOverStart >= TimeForHighlight)
                {
                    myButton.ShowHighlight();
                }
            }
        }
    }
}
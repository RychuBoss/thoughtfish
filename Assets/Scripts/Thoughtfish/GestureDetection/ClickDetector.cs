﻿using Thoughtfish.Views;
using UnityEngine;

namespace Thoughtfish.GestureDetection
{
    public class ClickDetector : MonoBehaviour
    {
        [SerializeField]
        private MyButton myButton;

        private void OnMouseOver()
        {
            if (Input.GetMouseButtonUp(0))
            {
                LeftClick();
            }
            if (Input.GetMouseButtonUp(1))
            {
                RightClick();
            }
        }

        private void LeftClick()
        {
            myButton.SwitchToNextShape();
        }

        private void RightClick()
        {
            myButton.SwitchToNextColor();
        }
    }
}
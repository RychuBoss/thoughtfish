﻿using Thoughtfish.Events;
using Thoughtfish.Views;
using UnityEngine;
using UnityEngine.Assertions;

namespace Thoughtfish.GestureDetection
{
    public class DragDetector : MonoBehaviour
    {
        private const float MinDragDistance = 3f;

        [SerializeField]
        private MyButton button;

        private Vector3 initialMousePosition;
        private Vector3 offset;
        private bool dragStarted;

        private Camera currentCamera;

        private void Awake()
        {
            currentCamera = Camera.main;
            Assert.IsNotNull(currentCamera);

        }

        private void OnMouseDown()
        {
            if (Input.GetMouseButtonDown(0))
            {
                initialMousePosition = GetMappedMousePosition();
                offset = transform.position - initialMousePosition;
                dragStarted = false;
            }
        }

        private void OnMouseDrag()
        {
            var mappedMousePosition = GetMappedMousePosition();

            if (dragStarted || Vector3.Distance(initialMousePosition, mappedMousePosition) >= MinDragDistance)
            {
                dragStarted = true;
                button.SetDragSpritePosition(mappedMousePosition + offset);
            }
        }

        private void OnMouseUp()
        {
            if (dragStarted)
            {
                dragStarted = false;
                var targetPosition = GetMappedMousePosition() + offset;
                SendShapeMovedEvent(targetPosition);
                button.MoveToPosition(targetPosition);
            }
        }

        private void SendShapeMovedEvent(Vector3 targetPosition)
        {
            var shapedMovedEventData = new ShapedMovedEventData
            {
                time = Time.time,
                shapeId = button.GetId(),
                initialPosition = button.transform.position,
                targetPosition = targetPosition
            };
            ShapeMovedEvent.Invoke(shapedMovedEventData);
        }

        private Vector3 GetMappedMousePosition()
        {
            var mousePosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
            return currentCamera.ScreenToWorldPoint(mousePosition);
        }
    }
}